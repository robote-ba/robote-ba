Hallo zusammen,

herzlich willkommen! Viel Erfolg im Wintersemester für uns alle!

hier sind die Links vom Herrn Pflaum.

1.NVIDIA Robotics: Isaac SDK and Sim 2020.1

https://youtu.be/v_ulHMfDUco

2.NVIDIA Isaac SDK 2019.3 for Robotics and Autonomous Machines

https://youtu.be/zPuTfBnqcsw

3.What is ROS (Robot Operating System)| Introduction to the Tutorials

https://youtu.be/N6K2LWG2kRI

4.Jetson RACECAR Part 11 - Arduino Car Controller

https://youtu.be/z2FLrzj3330

5.Autonomous Mobile Robot Navigation With ROS (Jetson Nano + Arduino + Lid...

https://youtu.be/K7pqndofT_A

6.JetsonHacks
https://youtube.com/c/JetsonHacks

7.Jetson AI Labs - E07 - NVIDIA Interns (August 26, 2021)
https://youtu.be/mFQY2XySZ0w

Liebe Grüße 

Huihui